function cacheFunction(cb) {

    if(arguments.length === 0 || typeof(arguments[0]) !== 'function'){
        throw new Error("missing some Arguments ");
    }

    const obj = {};
    // invokeFunction(arr);
    function invokeFunction(...args) {
        if(obj[args] === undefined){
            obj[args] = cb(...args);
            return obj[args];
        }else{
            return obj[args];
        }
        // console.log(cb(key, value));
        // invokeFunction()

    }
    // invokeFunction()
    return invokeFunction;
}

module.exports = cacheFunction;

