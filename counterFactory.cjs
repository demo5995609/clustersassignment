function counterFactory() {
    let num = 0;
    //Creating objects of increment and decrement function
    let obj = {
        increment : function(){
            num += 1;
            return num;
        },
        decrement : function(){
            num -= 1;
            return num;
        }
    }

    // returning Object
    return obj;
}

module.exports = counterFactory;