let cacheFunction = require('./cacheFunction.cjs');
// let invokeFunction = Func['invokeFunction']
// let cacheFunction = Func['cacheFunction']

function callBack(...args) {
    return arguments[0] + arguments[1];
}
// cacheFunction(callBack);
// let inputObj = [2, 3, 4, 5];
let ans = cacheFunction(callBack);
console.log(ans(3,4));
console.log(ans(2,6));
console.log(ans(3,6));
console.log(ans(6,6));

// let res = invoke